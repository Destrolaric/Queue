package queue;

public class LinkedQueue extends AbstractQueue {
    private Node headNode;
    private Node tailNode = null;
    private int tail = 0;
    private int head = 0;

    @Override
    public void enqueue(Object val) {
        if (headNode == null) {
            headNode = new Node(val);
            headNode.value = val;
            tailNode = headNode;
            tail++;
        } else {
            tailNode.nextNode = new Node(val);
            tailNode = tailNode.nextNode;
            tail++;
        }
    }

    @Override
    public Object element() {
        return headNode.value;
    }

    @Override
    public void clear() {
        headNode = null;
        tailNode = null;
        tail = head;
        System.gc();
    }

    @Override
    public int size() {
        return tail - head;
    }

    @Override
    public Object dequeue() {
        if (headNode == null) {
            return null;
        }
        if (headNode == tailNode) {
            Object val = headNode.value;
            head = tail;
            headNode = null;
            tailNode = null;
            return val;
        }
        Object val = headNode.value;
        head++;
        headNode = headNode.nextNode;
        return val;
    }
}
