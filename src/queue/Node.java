package queue;

public class Node {
    Object value;
    Node nextNode;

    public Node(Object data) {
        value = data;
        nextNode = null;
    }
}
