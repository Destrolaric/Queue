package queue;

public class ArrayQueue extends AbstractQueue {

    private int arraySize = 2;
    private Object[] array = new Object[arraySize];
    private int head = 0;
    private int tail = 0;

    public void addCap() {
        int newArraySize = arraySize *2;
        int savedSize = size();

        Object[] newArray = new Object[newArraySize];

        System.arraycopy(array, head, newArray, 0, array.length - head);
        System.arraycopy(array, 0, newArray, array.length - head, tail + 1);

        array = newArray;
        head = 0;
        tail = savedSize;
        arraySize = newArraySize;
    }

    @Override
    public int size() {
        return head > tail ? arraySize - head + tail : tail - head;
    }

    @Override
    public void clear() {
        head = 0;
        tail = 0;
    }

    @Override
    public void enqueue(Object object) {
        if (arraySize - 1 == size()) {
            addCap();
        }
        array[tail] = object;
        tail = (tail + 1) % arraySize;

    }

    @Override
    public Object element() {
        if (!isEmpty()) {
            return array[head];
        } else {
            return null;
        }
    }

    @Override
    public Object dequeue() {
        if (isEmpty()) {

            return null;
        }
        Object result=element();
        head = (head + 1) % arraySize;
        return result;
    }
}
